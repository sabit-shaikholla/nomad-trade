# NomadTrade E-Commerce System

This project is an E-Commerce platform for Kazakhstan market, designed to facilitate online buying and selling activities.

### Overview

The NomadTrade platform provides the following functionalities:

- User registration: Users can register an account to access the platform.
- Product browsing: Users can search for products and view detailed information about them.
- Order management: Sellers can manage incoming orders, update order statuses, and view order details.
- Feedback and reviews: Users can leave feedback and reviews for products and sellers.

### ER Diagram
[ER Diagram](src/main/java/org/trade/nomad/adr/ER-Diagram.md)

The ER Diagram for this project is visualized using PlantUML. You can find the PlantUML code in the ER-Diagram.puml file.

### Use Cases
- [Use Case Diagram](src/main/java/org/trade/nomad/adr/UseCases.md)
- [Use Case Details](src/main/java/org/trade/nomad/adr/ADR-README.md)

The Use Cases for this project are visualized using PlantUML. You can find the PlantUML code in the UseCases.puml file.